#!/usr/bin/env python3


def summe(summand_1, summand_2):
    return summand_1 + summand_2


def differenz(minuend, subtrahend):
    return minuend - subtrahend


def produkt(multiplikator, multiplikant):
    return multiplikator * multiplikant


def quotient(dividend, divisor):
    return dividend / divisor


if __name__ == "__main__":
    # 1) Nachfragen, was zu tun ist
    print("Hallo! Keine Lust auf Kopfrechnen? Dann mach ich das für dich!")
    operator = input("Was soll ich rechnen, +, -, * oder / ?: ")
    zahl_1_text = input("Nenne mir die erste Zahl: ")
    zahl_2_text = input("Nenne mir die zweite Zahl: ")
    # 2) Daten verarbeiten
    zahl_1 = float(zahl_1_text)
    zahl_2 = float(zahl_2_text)
    ergebnis = 0.0
    if operator == "+":
        ergebnis = summe(zahl_1, zahl_2)
    elif operator == "-":
        ergebnis = differenz(zahl_1, zahl_2)
    elif operator == "*":
        ergebnis = produkt(zahl_1, zahl_2)
    elif operator == "/":
        ergebnis = quotient(zahl_1, zahl_2)
    else:
        print(operator, " kenne ich nicht :(")
    # 3) Die verarbeiten Daten dem Benutzer mitteilen
    print("das Ergebnis lautet ", ergebnis)
